var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var jade = require('gulp-jade');
var webserver = require('gulp-webserver');
var notify = require('gulp-notify');
var wait = require('gulp-wait');

// compile jade templates
gulp.task('jade-compile', function() {

  gulp.src('./src/jade/*.jade')
    .pipe(jade({
        pretty: true
    }))
    .pipe(gulp.dest('./build'))
    .pipe(notify({
        title: 'Jade cooked',
        message: 'Succesful compiled',
        sound: true
    }));
});

// compile scss to css, add prefixes, minify and concat all css files
gulp.task('sass-compile', function () {

    gulp.src('./src/scss/style.scss')
      .pipe(wait(500))
      .pipe(sass({
          outputStyle: 'expanded'
      }))
      .on('error', sass.logError)
      .pipe(autoprefixer({
          browsers: ['last 2 versions', 'IE >= 9'],
          cascade: false
      }))
      .pipe(concat('bundle.css'))
      .pipe(gulp.dest('./build/css'))
      .pipe(csso())
      .pipe(concat('bundle.min.css'))
      .pipe(gulp.dest('./build/css'))
      .pipe(notify({
          title: 'SCSS cooked',
          message: 'File bundle.min.css ready for use',
          sound: true
      }));
});

// concat and minify js files
gulp.task('js-compile', function() {

    gulp.src(['./src/js/plugins/*.js', './src/js/functions.js', './src/js/main.js'])
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('./build/js'))
    .pipe(uglify())
    .pipe(concat('bundle.min.js'))
    .pipe(gulp.dest('./build/js'))
    .pipe(notify({
        title: 'JS cooked',
        message: 'File bundle.min.js ready for use',
        sound: true
    }));
});

// watch for changes in files and cook them
gulp.task('watch', function () {

    gulp.watch('./src/scss/*.scss', ['sass-compile']);
    gulp.watch('./src/jade/*.jade', ['jade-compile']);
    gulp.watch('./src/js/*.js', ['js-compile']);
});

//start local webserver (with `watch` task)
gulp.task('serve', ['watch'], function() {
  gulp.src('build')
    .pipe(webserver({
        port: 8080
    }));
});
