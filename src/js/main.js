$ = jQuery;

$(document).on('ready', function() {

	changeHeader();

	// init clouds parallax
	if ($(window).innerWidth() > 1020) {
		skrollr.init({
			forceHeight: false
		});
	}

	// init sliders
	$('.portfolio-slider').slick({
		slidesToShow: 1,
		prevArrow: '<span class="clients-arrow portfolio-arrow clients-arrow-prev portfolio-arrow-prev"></span>',
		nextArrow: '<span class="clients-arrow portfolio-arrow clients-arrow-next portfolio-arrow-next"></span>'
	});

	$('.portfolio-slide-single').each(function() {

		var $photos = $(this).find('.portfolio-slide-single-photos');

		$(this).find('.portfolio-slide-single-photos-slider').slick({
			slidesToShow: 1,
			appendArrows: $photos,
			prevArrow: '<span class="photos-arrow photos-arrow-prev"></span>',
			nextArrow: '<span class="photos-arrow photos-arrow-next"></span>'
		});
	});

	$('.about-slider-slides').slick({
		slidesToShow: 3,
		centerMode: true,
		variableWidth: true,
		prevArrow: '<span class="about-arrow about-arrow-prev"></span>',
		nextArrow: '<span class="about-arrow about-arrow-next"></span>',
		responsive: [
			{
				breakpoint: 1020,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					variableWidth: false
				}
			}
		]
	});

	$('.clients-photos-slider').slick({
		slidesToShow: 1,
		arrows: false,
		asNavFor: '.clients-slider',
		fade: true
	});

	$('.clients-slider').slick({
		slidesToShow: 1,
		asNavFor: '.clients-photos-slider',
		fade: true,
		prevArrow: '<span class="clients-arrow clients-arrow-prev"></span>',
		nextArrow: '<span class="clients-arrow clients-arrow-next"></span>'
	});

	$('.team-slider').slick({
		slidesToShow: 3,
		prevArrow: '<span class="clients-arrow team-arrow clients-arrow-prev team-arrow-prev"></span>',
		nextArrow: '<span class="clients-arrow team-arrow clients-arrow-next team-arrow-next"></span>',
		responsive: [
			{
				breakpoint: 1020,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	$('input[type="tel"]').inputmask({
  	    mask: '+7 (999) 999-99-99',
  	    showMaskOnHover: false,
  	    showMaskOnFocus: true
  	});

	//init map in contacts
    ymaps.ready(function() {

        var addresses = [
        	{
        		title: 'Новосибирск, ул. Войкова, 131',
        		coords: [55.059405, 82.944531]
        	},
        	{
        		title: 'Новосибирск, ул. Авиастроителей, 2/2 г',
        		coords: [55.069032, 82.966466]
        	}
        ];

        map = new ymaps.Map("map-container", {
	        center: [55.063283, 82.970501],
	        zoom: 13,
	    }, { suppressMapOpenBlock: true });

	    map.controls.add('zoomControl');

	    placemark1 = new ymaps.Placemark(addresses[0].coords, {
	        hintContent: addresses[0].title
	    }, {
	        iconLayout: "default#image",
	        iconImageHref: '/wp-content/themes/nursery-a/img/icon-map-1.png',
	        iconImageSize: [205, 55],
	        iconImageOffset: [-27, -55]
	    });

	    placemark2 = new ymaps.Placemark(addresses[1].coords, {
	        hintContent: addresses[1].title
	    }, {
	        iconLayout: "default#image",
	        iconImageHref: '/wp-content/themes/nursery-a/img/icon-map-2.png',
	        iconImageSize: [275, 55],
	        iconImageOffset: [-27, -55]
	    });

	    map.geoObjects.add(placemark1);
	    map.geoObjects.add(placemark2);
    });
});

$(window).on('scroll', function() {

	changeHeader();

	if ($(window).innerWidth() > 1020) {
		$('.block-intro').css('background-position-y', ($(window).scrollTop() / 1.2) + 'px');
		$('.intro-clouds .cloud').each(function() {
			$(this).css('transform', 'translateY(-' + ($(window).scrollTop() / 8) + 'px');
		});
	}
});

// init rockets offseting on mousemove
$('.block-intro, header').on('mousemove', function(event) {

	if ($(window).innerWidth() <= 1020) {
		return false;
	}

	var breakСoefficient = 20;
	var rocketsSelfCoefficientX = [-5, 9, -6, 5];
	var rocketsSelfCoefficientY = [6, -2, 7, 11];

	var offsetX = event.pageX / breakСoefficient;
	var offsetY = event.pageY / breakСoefficient;

	$('.rocket').each(function(index) {
		$(this).css('transform', 'translate(' + offsetX * rocketsSelfCoefficientX[index] + 'px, ' + offsetY * rocketsSelfCoefficientY[index] + 'px');
	});
});

// scroll to block
$('.header-menu a, .faq-single-answer a').on('click', function() {

	var target = $(this).attr('href');

	if (typeof target !== 'undefined') {

		if (target != '#advantages') {
			var offsetSize = $(target).offset().top - 80;
		} else {
			var offsetSize = $(target).offset().top + 300;
		}

		$('html, body').animate({
			scrollTop:  offsetSize + 'px'
		}, 1000);
	}

	return false;
});

// initialize popups
$('a.intro-video-inner').magnificPopup({
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 160,
	preloader: false,
	fixedContentPos: true
});

$('.popup').on('click', function() {

	var target = $(this).attr('href');
	var index = $(this).attr('data-index');

	if (typeof target !== 'undefined') {
		$.magnificPopup.open({
		  items: { src: target },
		  type: 'inline',
		  mainClass: 'form-modal',
		  showCloseBtn: false,
		  closeOnBgClick: false,
		  callbacks: {
		  	open: function() {
		  		var modalName = $('.mfp-content .modal').attr('id');
		  		var items = ['menu', 'thanks', 'sale-thanks'];

		  		if (items.indexOf(modalName) != -1) {
		  			$('.mfp-bg').addClass('white-bg');
		  		}

		  		if (modalName.indexOf('team') != -1 || modalName.indexOf('files') != -1) {
		  			$('.mfp-bg').addClass('no-bg');
		  		}

		  		// files slider and scroll to clicked photo
		  		if (typeof index !== "undefined") {
		  			if (!$('.modal-files-slider').hasClass('slick-initialized')) {
			  			$('.modal-files-slider').slick({
			  				slidesToShow: 1,
			  				prevArrow: '<span class="files-arrow files-arrow-prev"></span>',
			  				nextArrow: '<span class="files-arrow files-arrow-next"></span>'
			  			});
			  		}
		  			$('.modal-files-slider').slick('slickGoTo', parseInt(index));
		  		}
		  	}
		  }
		});
	}

	return false;
});

$('.modal-close, .modal-team-close, .thanks-close').on('click', function() {
	$.magnificPopup.close();
});

$('.header-menu-burger').on('click', function() {

	$('header').toggleClass('opened');
	$('.header-menu').slideToggle();
});

// fix for highlight active label
$('.intro-form-b input[type=tel], .intro-form-b input[type=text]').on('focus', function() {
	$(this).closest('label').addClass('active');
});
$('.intro-form-b input[type=tel], .intro-form-b input[type=text]').on('blur', function() {
	$(this).closest('label').removeClass('active');
});

// show/hide faq's answers
$('.faq-single').on('click', function() {

	var $faqBlock = $(this);
	var $answerBlock = $(this).find('.faq-single-answer');

	if ( ! $faqBlock.hasClass('opened') ) {
		$faqBlock.addClass('opened');
		$faqBlock.find('.faq-single-question-switcher').text('свернуть ответ');
		$answerBlock.slideDown(300);
	} else {
		$faqBlock.removeClass('opened');
		$faqBlock.find('.faq-single-question-switcher').text('развернуть ответ');
		$answerBlock.slideUp(300);
	}
});

// numbers animate from 0 to neccessary value
$('.advantages2-single-b').eq(0).on('scrollSpy:enter', function() {

    if (!$(this).hasClass('showed')) {
        $(this).addClass('showed');
        $({someValue: 0}).animate({someValue: 11}, {
            duration: 2000,
            easing:'swing',
            step: function() {
                $('.advantages2-single-number-1-b').text(Math.round(this.someValue));
            }
        });
        $({someValue1: 0}).animate({someValue1: 520}, {
            duration: 2000,
            easing:'swing',
            step: function() {
                $('.advantages2-single-number-2-b').text(Math.round(this.someValue1));
            }
        });
        $({someValue2: 0}).animate({someValue2: 13}, {
            duration: 2000,
            easing:'swing',
            step: function() {
                $('.advantages2-single-number-3-b').html(Math.round(this.someValue2));
            }
        });
    }
});

$('.advantages2-single-b').eq(0).scrollSpy();

// change header from absolute to fixed (fixed to absolute)
function changeHeader() {

    var headerHeight = $('.block-intro').innerHeight();
    var scrollPos = $(window).scrollTop();

    if (scrollPos >= headerHeight - 100) {
        $('header').addClass('fixed');
    } else {
        $('header').removeClass('fixed');
    }
}

// timer's functions
function getTimeRemaining(endtime) {

  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {

  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days-num');
  var hoursSpan = clock.querySelector('.hours-num');
  var minutesSpan = clock.querySelector('.minutes-num');
  var secondsSpan = clock.querySelector('.seconds-num');

  var daysDesc = clock.querySelector('.days-desc');
  var hoursDesc = clock.querySelector('.hours-desc');
  var minutesDesc = clock.querySelector('.minutes-desc');
  var secondsDesc = clock.querySelector('.seconds-desc');

  function updateClock() {

    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    daysDesc.innerHTML = declOfNum(t.days, ['день', 'дня', 'дней']);
    hoursDesc.innerHTML = declOfNum(t.hours, ['час', 'часа', 'часов']);
    minutesDesc.innerHTML = declOfNum(t.minutes, ['минута', 'минуты', 'минут']);
    secondsDesc.innerHTML = declOfNum(t.seconds, ['секунда', 'секунды', 'секунд']);

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

function declOfNum(number, titles) {

	cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}